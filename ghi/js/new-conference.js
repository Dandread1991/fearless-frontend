window.addEventListener('DOMContentLoaded', async () => {

    const formTag = document.getElementById("create-conference-form")

    const url = "http://localhost:8000/api/locations/"
    try {
        const  response = await fetch(url)


    if (!response.ok) {
        throw Error("Error!")
    } else {
        const data = await response.json()
        let select = document.getElementById("location")

        for (let location of data.locations) {

            const locationOption = document.createElement("option")
            locationOption.value = location.id
            locationOption.innerHTML += location.name

            select.appendChild(locationOption)


        }
    }

    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)

        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
                    },

};


    const conferenceResponse = await fetch(locationUrl, fetchConfig)

    if (conferenceResponse.ok)
    {
        formTag.reset()
        const newConference = await conferenceResponse.json()

    }


    })



 }
  catch (e){

    }


})
