window.addEventListener('DOMContentLoaded', async () => {

    const formTag = document.getElementById("create-location-form")

    const url = "http://localhost:8000/api/states/"
    try {
        const  response = await fetch(url)


    if (!response.ok) {
        throw Error("Error!")
    } else {
        const data = await response.json()
        let select = document.getElementById("state")

        for (let state of data.states) {

            const stateOption = document.createElement("option")
            stateOption.value = state.abbreviation
            stateOption.innerHTML += state.name

            select.appendChild(stateOption)


        }
    }

    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)

        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
                    },

};

    const locationResponse = await fetch(locationUrl, fetchConfig)

    if (locationResponse.ok)
    {
        formTag.reset()
        const newLocation = await locationResponse.json()

    }


    })



 }
  catch (e){

    }


})
