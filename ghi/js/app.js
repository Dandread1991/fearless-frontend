function createCard(name, description, pictureURL,startDate,endDate,location) {
    return `<div class="shadow-lg rounded">
    <div class="card mb-3 border-dark shadow-lg p-3" style="max-width: 15rem;">
    <img src = "${pictureURL}" class="card-img-top">
    <div class = "card-body">
    <h5 class = "card-title">${name}</h5>
    <h6 class = "card-subtitle mb-2 text-muted">${location}</h6>
    <p class="card-text">${description}</p>
    <div class="card-footer text-body-secondary">${startDate}-${endDate}
    </div>
    </div>
    </div>`
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/"
    let num = 1
    try {
    const response = await fetch(url)

    if(!response.ok) {
            throw Error("Error, Invalid response" )
    } else {


    const data = await response.json()
    for (let conference of data.conferences) {
        const detailURL = `http://localhost:8000${conference.href}`
        const detailResponse = await fetch(detailURL)



        if (detailResponse.ok){
            const details = await detailResponse.json()
            const title = details.conference.name
            const description = details.conference.description
            const pictureURL = details.conference.location.picture_url
            const startDate = new Date(details.conference.starts).toLocaleDateString()
            const endDate = new Date(details.conference.ends).toLocaleDateString()
            const location = details.conference.location.name

            const html = createCard(title, description, pictureURL,startDate,endDate,location)


            let column = document.querySelector(`#col${num}`)
            column.innerHTML += html
            num++





        }
        if (num > 3) {
            num = 1
        }


    }
    }
} catch (e) {
    const errorHandle = document.querySelector('body')

    errorHandle.innerHTML +=`<div class = "alert alert-primary" role="alert">${e}</div>`
}
});
